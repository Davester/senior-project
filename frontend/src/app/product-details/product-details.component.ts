import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.Model';
import { User } from '../models/User.Model';

import { ProductService } from '../product.service';
import { UserService } from '../user.service';
import { ActivatedRoute } from '@angular/router';
import { CartService } from '../cart.service';
@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.css']
})
export class ProductDetailsComponent implements OnInit {

  constructor(private productService: ProductService, private userService: UserService, private route: ActivatedRoute, private cartService: CartService) { }
  product: Product;
  user: any;
  ngOnInit() {
    this.route.params.subscribe((params) => {
      const id = params['id'];
      this.productService.getById(id).subscribe(value => {
        this.product = value;
        this.userService.getByID(value.seller).subscribe(user => {
          this.user = user;
        });
      }
      );

    });
  }

}

export interface User {
    username: string;
    email: string;
    name: string;
    age: Number;
    created: Date;
    _id: string;
}
export class Product {
    _id: string;
    name: string;
    price: number;
    seller: string;
    category: string;
    description: string;
    pictures: string[];
}

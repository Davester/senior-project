import { Component, OnInit } from '@angular/core';
import { RegisterUser } from '../models/Register_user.Model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../user.service';
import { UrlSerializer } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: RegisterUser = new RegisterUser();
  registerForm: FormGroup;
  hide = true;
  message = null;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      'name': [this.user.name, [
      ]],
      'username': [this.user.username, [
        Validators.required,
        Validators.minLength(6)
      ]],
      'email': [this.user.email, [
        Validators.required,
        Validators.email
      ]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ]],
      'age': [this.user.age, [
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
        Validators.min(1)
      ]]
    });
  }

  onRegisterSubmit() {
    this.userService.register(this.user).subscribe(value => {
      this.router.navigate(['/login']);
    },
    err => {
      console.log(err);
      this.message = err.error.message;
    });
  }
}

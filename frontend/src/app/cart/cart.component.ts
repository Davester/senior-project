import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';
import { Product } from '../models/product.Model';
@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private cartService: CartService) { }
  products: Product[];
  totalValue;
  ngOnInit() {
    this.totalValue = 0;
    this.cartService.cartsub.asObservable().subscribe(val => {
    this.products = val;
      this.totalValue = 0;
      for (let i = 0; i < this.products.length; i++) {
        this.totalValue += this.products[i].price;
      }
    });

  }
  alert() {
    alert("placeholder");
  }
}

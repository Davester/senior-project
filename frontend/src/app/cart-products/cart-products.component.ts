import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart-products',
  templateUrl: './cart-products.component.html',
  styleUrls: ['./cart-products.component.css']
})
export class CartProductsComponent implements OnInit {

  constructor(private cartService: CartService) { }
  products;
  ngOnInit() {
    this.products = this.cartService.cartsub.asObservable();
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../product.service';
import { Product } from '../models/product.Model';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private productService: ProductService, private router: Router, private route: ActivatedRoute) { }
  productForm: FormGroup;
  product = new Product();
  message: string;
  id: any;
  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
    });
    this.product.pictures = [''];
    this.productForm = this.formBuilder.group({
      'name': [this.product.name, [
        Validators.required
      ]],
      'description': [this.product.description, [
        Validators.required

      ]],
      'price': [this.product.price, [
        Validators.required,
        Validators.pattern(/^-?(0|[1-9]\d*)?$/),
        Validators.min(1)]],
      'category': [this.product.category, [
        Validators.required
      ]],
      'picture': [this.product.pictures[0], [
      ]]
    });
  }
  onproductSubmit() {
    this.product._id = this.id;
    this.productService.update(this.product).subscribe((val) => {
      this.router.navigate([`/products/${this.id}`]);
    },
      err => {
        console.log(err);
        this.message = err.error.message;
      });
  }
}

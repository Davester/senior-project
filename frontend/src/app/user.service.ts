import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegisterUser } from './models/Register_user.Model';
import { User } from './models/User.Model';
import { Subject, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly ROOT_URL = 'http://localhost:3000/users';
  currentUser: User;
  public loggedIn = new BehaviorSubject<boolean>(false);
  public loggedInObs = this.loggedIn.asObservable();
  public username = new BehaviorSubject<string>("");
  public usernameObs = this.username.asObservable();
  constructor(private http: HttpClient) {
    let user = localStorage.getItem('user');
    if (user) {
      console.log(user);

      this.currentUser = JSON.parse(user);
      console.log(this.currentUser);
       this.loggedIn.next(true);
       let name: string = this.currentUser.username;
       this.username.next(name);
    }
  }
  login = (username: string, password: string) => {
    console.log("got to login");
    return this.http.post<User>(this.ROOT_URL + '/login',
      { username: username, password: password }, { withCredentials: true, observe: 'response' }).pipe(
        map(value => {
          return {
            status: value.status,
            user: value.body
          };
        })
      );
  }
  get(username: string) {
    return this.http.get(this.ROOT_URL + `/get/${username}`, { withCredentials: true });
  }
  getByID(id: string) {
    return this.http.get(this.ROOT_URL + `/getbyid/${id}`, { withCredentials: true });
  }
  register(user: RegisterUser) {
    return this.http.post(this.ROOT_URL, { user: user }, { observe: 'response' });
  }
  setCurrentUser(user: User) {
    this.currentUser = user;
    localStorage.setItem("user", JSON.stringify(user));
    this.loggedIn.next(true);
    this.username.next(user.username.toString());
  }
  logout() {
    localStorage.removeItem('user');
    this.loggedIn.next(false);
    this.username.next("");
  }
  current() {
    return this.http.get<User>(this.ROOT_URL + '/current', { withCredentials: true });
  }

  updatePassword(oldpass: string, newpass: string) {
    return this.http.post(this.ROOT_URL + '/updatepassword', { oldpass, newpass });
  }
}

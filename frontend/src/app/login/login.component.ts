import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { LoginModel } from '../models/login.Model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) { }
  user: LoginModel = new LoginModel();
  loginForm: FormGroup;
  hide = true;
  message: string = null;
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      'username': [this.user.username, [
        Validators.required,
        Validators.minLength(6)
      ]],
      'password': [this.user.password, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ]]
    });
  }

  onLoginSubmit() {
    this.userService.login(this.user.username, this.user.password).subscribe(value => {
      if (value.status === 200) {
        console.log('response status 200');
        console.log(value.user);
        this.userService.setCurrentUser(value.user);
        this.router.navigate(['']);
      }
    },
      err => {
        console.log(err);
        this.message = 'Invalid Username and password combination';
      });
  }

}

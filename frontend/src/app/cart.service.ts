import { Injectable } from '@angular/core';
import { Product } from './models/product.Model';
import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CartService {
  cart: Product[] = [];
  cartsub: BehaviorSubject<Product[]> = new BehaviorSubject<Product[]>(this.cart);
  cartObs = this.cartsub.asObservable();
  constructor() {
    let cart = localStorage.getItem('cart');
    if (cart) {
      this.cart = JSON.parse(cart);

    }
    this.cartsub.next(this.cart);

  }
  public addProduct(product: any) {
    this.cart.push(product);
    localStorage.setItem('cart', JSON.stringify(this.cart));
    this.cartsub.next(this.cart);
    console.log('added product :');
    console.log(product);
    console.log(this.cart);
  }
  getCart() {
    let cart = localStorage.getItem('cart');
    if (cart) {
      this.cart = JSON.parse(cart);
    }
    this.cartsub.next(this.cart);
  }
  removeProduct(product: any) {
    const index = this.cart.findIndex(arrproduct => arrproduct === product);
    this.cart.splice(index, 1);
    localStorage.setItem('cart', JSON.stringify(this.cart));
    this.cartsub.next(this.cart);
  }
}

import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/User.Model';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  constructor(private userService: UserService) { }
  user: any;
  ngOnInit() {
    this.user = this.userService.current();
  }

}

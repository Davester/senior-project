var mongoose = require('mongoose');
var crypto = require('crypto');
var schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose')
mongoose.connect("mongodb://root:root@mongo:27017/admin")
var userSchema = new schema({
    name: String,
    email: {type: String, required: true,unique: true},
    admin: Boolean,
    age: Number,
    created: Date
});
userSchema.plugin(passportLocalMongoose);
var user = mongoose.model("user", userSchema);

module.exports = user;
var mongoose = require('mongoose');
var crypto = require('crypto');
var schema = mongoose.Schema;
var query = require('mongoose-string-query')
var user = require('./user.Service')
var passportLocalMongoose = require('passport-local-mongoose')
mongoose.connect("mongodb://root:root@mongo:27017/admin")
var productSchema = new schema({
    seller: { type: mongoose.Schema.Types.ObjectId, ref: 'user' },
    name: { type: String, required: true },
    price: { type: Number, required: true },
    category: { type: String, required: true },
    description: String,
    pictures: [String]
});
productSchema.pre("save", function preSave(next) {
    console.log(this.seller);
    console.log(this.name);
    user.findById(this.seller, (err, found) => {
        if (found) return next();
        else return next(new Error("no user found matching the ID"));
    });
});
productSchema.plugin(query);
var product = mongoose.model("product", productSchema);

module.exports = product;
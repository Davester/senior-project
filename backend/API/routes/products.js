var express = require('express');
var router = express.Router();
var passport = require('passport')
var product = require('../Services/product.Service');
var auth = require('../Services/authentication.Service')


router.get('/single/:id', function (req, res, next) {
    product.findById(req.params.id).populate('user', 'name').exec((err, result) => {
        if (err) {
            console.log(err);
            res.status(500);
            res.send(err);
        }
        else {
            if (result == null) {
                res.status(404);
                res.send();
            }
            else {
                res.send(result);
            }
        }
    })
});
router.post('/', auth, (req, res, next) => {
    var incproduct = req.body.product;
    var newProduct = new product({
        seller: req.user._id,
        name: incproduct.name,
        price: incproduct.price,
        category: incproduct.category,
        description: incproduct.description,
        pictures: incproduct.pictures
    });
    newProduct.save().then((result) => {
        res.send(result._id);
    }).catch((err) => {
        console.log('error while adding product!');
        console.log(err)
        res.status(500);
        return res.send(err);
    });
});
router.post(`/update`, auth, (req, res, next) => {
    productID = req.body.product._id;

    product.findById(productID, function (err, product) {
        if (err) {
            res.status(500);
            return res.send(err);
        };
        if (!product) {
            res.status(404);
            return res.send("product not found");
        }
        else {
            console.log(product)
            console.log("'" + escape(req.user._id) + "'");
            console.log("'" + escape(product.seller) + "'");
            var seller = escape(product.seller);
            var user = escape(req.user._id);
            if (seller != user) {

            }
            product.set(req.body.product);
            product.save(function (err, updatedproduct) {
                if (err) {
                    res.status(500);
                    return res.send(err);
                } else res.send(updatedproduct);
            });
        }
    });
})

router.get('/categories', (req, res, next) => {
    product.find().distinct('category', function (err, categories) {
        if (err) {
            console.log(err);
            res.status(500);
            return res.send(err);
        }
        else return res.send(categories);

    })
});
router.delete('/:id', auth, (req, res, next) => {
    console.log(req.params.id);
    product.findById(req.params.id).then((resproduct) => {
        if (resproduct) {
            if (escape(resproduct.seller) != escape(req.user._id)) {
                res.status(401);
                return res.send({ error: "unauthorized to update this product" });
            }
            else {
                resproduct.remove();
                return res.send({ message: "ok" });
            };
        }
        else {
            res.status(404);
            return res.send({message: "product not found"});
        }

    },(err) => {
        if(err){
            res.status(500);
            res.send(err);
        }
    })
});
router.get('/query', (req, res, next) => {
    product.apiQuery(req.query, function (err, products) {
        if (err) {
            res.status(500);
            return res.send({ error: err });
        }
        else return res.send(products);
    })
})
module.exports = router;